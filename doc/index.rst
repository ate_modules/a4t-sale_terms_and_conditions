Terms & Conditions of Sale
##########################

Adds the possibility to define the default Terms & Conditions of Sale as
well as specific Terms & Conditions of Sale to third parties.

.. important:: The default sales report is overrided by the sample Sale report
in this module.
